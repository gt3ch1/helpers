/*
* @author Gavin Pease, 2019
* 
*/
package me.gt3ch1.helpers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataDumper<DATA, TIME> {
	private DATA data;
	private TIME time;
	private String filename;
	private File f;

	/**
	 * public DataDumper(DATA d, TIME t, String filename) This method takes in
	 * parameters d and t, and String filename, and it then initializes the class to
	 * be used later.
	 * 
	 * @param d
	 * @param t
	 * @param filename
	 */
	public DataDumper(DATA d, TIME t, String filename) {
		setDataType(d);
		setTime(t);
		setFileName(filename);
	}

	/**
	 * private void setFileName(String f) takes in a parameter f, and sets the
	 * filename to f
	 * 
	 * @param f
	 */
	private void setFileName(String f) {
		filename = f;
	}

	/**
	 * private void setDataType(DATA d) takes in parameter d, and sets the data type
	 * to d
	 * 
	 * @param d
	 */
	private void setDataType(DATA d) {
		data = d;
	}

	/**
	 * private void setDataType(TIME t) takes in parameter t, and sets the data type
	 * to t
	 * 
	 * @param t
	 */
	private void setTime(TIME t) {
		time = t;
	}

	/**
	 * public void createFile() creates a file in the directory that is defined in
	 * variable filename that is appended by the time of file creation.
	 */
	public void createFile() {
		Date d = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM_HH-mm-ss");

		String newFileStr = filename + "-" + formatter.format(d) + ".csv";
		f = new File(newFileStr);
		try {
			if (f.createNewFile()) {
				System.out.println("Data file: " + newFileStr + " Successfully created.");

			} else {
				System.out.println("Ya goofed up.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * public void addData(DATA data, TIME time) Takes in two parameters, data and
	 * time, whose types are defined when you first create the object, and appends
	 * the parameters passed to the method call to the file that was created.
	 * 
	 * @param data
	 * @param time
	 */
	public void addData(DATA data, TIME time) {
		try {

			BufferedWriter bw = new BufferedWriter(new FileWriter(f, true));
			bw.append(data + "," + time + "\r\n");
			bw.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
